select City.Name
from Country
  inner join (City inner join Capital on City.Id = Capital.CityId) 
  on City.CountryCode = Country.Code
where Country.Name like "Malaysia";
