select Country.Name, max(LiteracyRate.Rate) Rate
from 
  Country 
  inner join LiteracyRate 
  on LiteracyRate.CountryCode = Country.Code
  group by Country.name
having LiteracyRate.Year = max(LiteracyRate.Year)
order by LiteracyRate.Rate desc limit 1;
