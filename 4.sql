select Country.Name, count(City.Id) Cnt
from Country left join City on City.CountryCode = Country.Code and City.Population >= 1000000
group by Country.Name
order by Cnt desc;
